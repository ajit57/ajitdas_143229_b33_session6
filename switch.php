<?php
$i = 15;
switch($i){
    case 0:
        echo "the value of I is 0";
        break;
    case 1:
        echo "the value of I is 1";
        break;
    case 5:
        echo "the value of I is 5";
        break;
    case 10:
        echo "the value of I is 10";
        break;
    case 15:
        echo "the value of I is 15";
        break;
    case 20:
        echo "the value of I is 20";
        break;
    default:
        echo "the value of I is not 1, 5, 10, 15, 20";
}